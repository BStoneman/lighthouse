﻿using UnityEngine;
using System.Collections;

public class Modification
{
    public string name;
    protected ModificationType modType;

    protected enum ModificationType
    {
        Turret,
        Armour,
        Sensor,
    }
}