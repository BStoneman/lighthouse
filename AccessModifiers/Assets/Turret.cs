﻿using UnityEngine;
using System.Collections;

public class Turret : Modification
{

    readonly ModificationType part = ModificationType.Turret;
    public turretType type;

    public enum turretType
    {
        Mining,
        Torpedo,
        Laser,
    }
}
