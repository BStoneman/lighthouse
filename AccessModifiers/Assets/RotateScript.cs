﻿using UnityEngine;
using System.Collections;

public class RotateScript : MonoBehaviour
{
    public float speed = 10f;
    public float rotationSpeed = 80.0f;
    public bool rightSpin;
    public bool leftSpin;
    public bool upSpin;
    public bool downSpin;
    public bool orbit;

    void Update()
    {
        if (rightSpin)
        {
            transform.Rotate(Vector3.right, speed * Time.deltaTime);
        }

        if (leftSpin)
        {
            transform.Rotate(Vector3.left, speed * Time.deltaTime);
        }

        if (upSpin)
        {
            transform.Rotate(Vector3.up, speed * Time.deltaTime);
        }

        if (downSpin)
        {
            transform.Rotate(Vector3.down, speed * Time.deltaTime);
        }

        if (orbit)
        {
            transform.RotateAround(transform.parent.position, Vector3.left, rotationSpeed * Time.deltaTime);
            transform.RotateAround(transform.parent.position, Vector3.up, rotationSpeed * Time.deltaTime);
        }       
    }
}
